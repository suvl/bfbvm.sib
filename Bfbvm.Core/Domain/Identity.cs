﻿namespace Bfbvm.Core.Domain
{
    /// <summary>
    /// An identity representing a person
    /// </summary>
    public struct Identity
    {
        /// <summary>
        /// Gets or sets the full name.
        /// </summary>
        /// <value>
        /// The full name.
        /// </value>
        public string FullName { get; set; }

        /// <summary>
        /// Gets or sets the gender.
        /// </summary>
        /// <value>
        /// The gender.
        /// </value>
        public string Gender { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>
        /// The email.
        /// </value>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        /// <value>
        /// The address.
        /// </value>
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets the mobile.
        /// </summary>
        /// <value>
        /// The mobile.
        /// </value>
        public string Mobile { get; set; }

        /// <summary>
        /// Gets or sets the birthday.
        /// </summary>
        /// <value>
        /// The birthday.
        /// </value>
        public System.DateTime Birthday { get; set; }

        /// <summary>
        /// Gets or sets the image identifier.
        /// </summary>
        /// <value>
        /// The image identifier.
        /// </value>
        public string ImageId { get; set; }

        /// <summary>
        /// Gets or sets the national identity number.
        /// </summary>
        /// <value>
        /// The national identity number.
        /// </value>
        public string NationalIdentityNumber { get; set; }

        /// <summary>
        /// Gets or sets the fiscal number.
        /// </summary>
        /// <value>
        /// The fiscal number.
        /// </value>
        public string FiscalNumber { get; set; }

        /// <summary>
        /// Gets or sets the health insurance number.
        /// </summary>
        /// <value>
        /// The health insurance number.
        /// </value>
        public string HealthInsuranceNumber { get; set; }
    }
}