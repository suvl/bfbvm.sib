﻿using System;

namespace Bfbvm.Core.Domain
{
    /// <inheritdoc />
    /// <summary>
    /// A musician domain entity
    /// </summary>
    /// <seealso cref="Entity"/>
    public class Musician : Entity
    {
        /// <summary>
        /// Gets or sets the identity.
        /// </summary>
        /// <value>
        /// The identity.
        /// </value>
        public Identity Identity { get; set; }

        /// <summary>
        /// Gets or sets the enlist date.
        /// </summary>
        /// <value>
        /// The enlist date.
        /// </value>
        public DateTime? EnlistDate { get; set; }

        /// <summary>
        /// Gets or sets the parents.
        /// </summary>
        /// <value>
        /// The parents.
        /// </value>
        public ParentIdentity Parents { get; set; }

        /// <summary>
        /// Gets or sets the instrument.
        /// </summary>
        /// <value>
        /// The instrument.
        /// </value>
        public string Instrument { get; set; }
    }
}