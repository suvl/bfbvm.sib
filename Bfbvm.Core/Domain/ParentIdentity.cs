﻿namespace Bfbvm.Core.Domain
{
    /// <summary>
    /// Identity information about parents
    /// </summary>
    public class ParentIdentity
    {
        /// <summary>
        /// Gets or sets the name of the father.
        /// </summary>
        /// <value>
        /// The name of the father.
        /// </value>
        public string FatherName { get; set; }

        /// <summary>
        /// Gets or sets the father mobile.
        /// </summary>
        /// <value>
        /// The father mobile.
        /// </value>
        public string FatherMobile { get; set; }

        /// <summary>
        /// Gets or sets the name of the mother.
        /// </summary>
        /// <value>
        /// The name of the mother.
        /// </value>
        public string MotherName { get; set; }

        /// <summary>
        /// Gets or sets the mother mobile.
        /// </summary>
        /// <value>
        /// The mother mobile.
        /// </value>
        public string MotherMobile { get; set; }

        /// <summary>
        /// Gets or sets the parent email.
        /// </summary>
        /// <value>
        /// The parent email.
        /// </value>
        public string ParentEmail { get; set; }
    }
}