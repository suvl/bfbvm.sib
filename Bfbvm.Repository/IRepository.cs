﻿using System.Threading;
using System.Threading.Tasks;
using Bfbvm.Core.Domain;

namespace Bfbvm.Repository
{
    /// <summary>
    /// A repository of <see cref="Entity"/>
    /// </summary>
    /// <typeparam name="T">The entity's concrete type</typeparam>
    public interface IRepository<T> where T : Entity
    {
        /// <summary>
        /// Gets by the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="ctx">The CTX.</param>
        /// <returns>The entity.</returns>
        Task<T> Get(string id, CancellationToken ctx = default);

        /// <summary>
        /// Inserts the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="ctx">The CTX.</param>
        /// <returns>.</returns>
        Task Insert(T entity, CancellationToken ctx = default);

        /// <summary>
        /// Updates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="ctx">The CTX.</param>
        /// <returns>.</returns>
        Task Update(T entity, CancellationToken ctx = default);

        /// <summary>
        /// Deletes the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="ctx">The CTX.</param>
        /// <returns>.</returns>
        Task Delete(T entity, CancellationToken ctx = default);

        /// <summary>
        /// Deletes by the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="ctx">The CTX.</param>
        /// <returns>.</returns>
        Task Delete(string id, CancellationToken ctx = default);
    }
}