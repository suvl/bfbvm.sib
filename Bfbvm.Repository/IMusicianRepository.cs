﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Bfbvm.Core.Domain;

namespace Bfbvm.Repository
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Repository.IRepository{T}" />
    public interface IMusicianRepository : IRepository<Musician>
    {
        /// <summary>
        /// Gets all musicians.
        /// </summary>
        /// <param name="ctx">The CTX.</param>
        /// <returns>The list of musicians.</returns>
        Task<List<Musician>> GetAllMusicians(CancellationToken ctx = default);
    }
}