﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Bfbvm.Core.Domain;
using Bfbvm.Core.Exceptions;
using Microsoft.Extensions.Logging;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Bfbvm.Repository
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class MongoRepository<T> : IRepository<T> where T : Entity
    {
        private readonly ILogger _logger;

        /// <summary>
        /// The collection
        /// </summary>
        protected readonly IMongoCollection<T> Collection;

        /// <summary>
        /// The filters
        /// </summary>
        protected static readonly FilterDefinitionBuilder<T> Filters
            = new FilterDefinitionBuilder<T>();

        /// <summary>
        /// Initializes a new instance of the <see cref="MongoRepository{T}"/> class.
        /// </summary>
        /// <param name="logger">The logger.</param>
        /// <param name="connectionString">The connection string.</param>
        /// <exception cref="ArgumentNullException">
        /// logger
        /// or
        /// connectionString
        /// </exception>
        protected MongoRepository(ILogger logger, string connectionString)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            if (string.IsNullOrEmpty(connectionString)) throw new ArgumentNullException(nameof(connectionString));

            var mongoUrl = new MongoUrl(connectionString);
            var client = new MongoClient(mongoUrl);
            var database = client.GetDatabase(mongoUrl.DatabaseName, new MongoDatabaseSettings
            {
                GuidRepresentation = GuidRepresentation.Standard,
                ReadPreference = ReadPreference.Nearest,
                ReadEncoding = new UTF8Encoding(),
                WriteConcern = WriteConcern.Acknowledged,
                WriteEncoding = new UTF8Encoding()
            });
            var collection = database.GetCollection<T>(typeof(T).Name, new MongoCollectionSettings
            {
                AssignIdOnInsert = true
            });

            Task.Run(() => SetupIndexes(collection));

            Collection = collection;

            _logger.LogInformation($"MongoRepository<{typeof(T).Name} .ctor");
        }

        /// <summary>
        /// Setups the indexes.
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <returns></returns>
        protected virtual Task SetupIndexes(IMongoCollection<T> collection)
        {
            // By default, Id is already indexed. Nothing to do.
            _logger.LogDebug("Base SetupIndexes");
            return Task.CompletedTask;
        }

        /// <inheritdoc />
        /// <summary>
        /// Gets by the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="ctx">The CTX.</param>
        /// <returns>The entity.</returns>
        public Task<T> Get(string id, CancellationToken ctx = default)
        {
            if (string.IsNullOrEmpty(id)) throw new ArgumentNullException(nameof(id));
            _logger.LogDebug("Getting by id {0}", id);
            return Collection.Find(Filters.Eq("_id", id)).SingleOrDefaultAsync(ctx);
        }

        private readonly InsertOneOptions _insertOneOptions =
            new InsertOneOptions { BypassDocumentValidation = false };

        /// <inheritdoc />
        /// <summary>
        /// Inserts the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="ctx">The CTX.</param>
        /// <returns>.</returns>
        public Task Insert(T entity, CancellationToken ctx = default)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            _logger.LogInformation("Inserting {0}", entity);
            return Collection.InsertOneAsync(entity, _insertOneOptions, ctx);
        }

        private readonly UpdateOptions _updateOptions = new UpdateOptions
        {
            IsUpsert = false,
            BypassDocumentValidation = false
        };

        /// <inheritdoc />
        /// <summary>
        /// Updates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="ctx">The CTX.</param>
        /// <returns>.</returns>
        public async Task Update(T entity, CancellationToken ctx = default)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            _logger.LogInformation("Updating entity {0}", entity);
            ReplaceOneResult result = await Collection.ReplaceOneAsync(
                Filters.Eq("_id", entity.Id),
                entity,
                _updateOptions,
                ctx);
            if (result.IsAcknowledged && result.ModifiedCount == 1) return;

            throw new SibRepositoryException($"Unable to update entity {entity}");
        }

        /// <inheritdoc />
        /// <summary>
        /// Deletes the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="ctx">The CTX.</param>
        /// <returns>.</returns>
        public Task Delete(T entity, CancellationToken ctx = default)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            return Delete(entity.Id, ctx);
        }

        /// <inheritdoc />
        /// <summary>
        /// Deletes by the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="ctx">The CTX.</param>
        /// <returns>.</returns>
        public async Task Delete(string id, CancellationToken ctx = default)
        {
            if (string.IsNullOrEmpty(id)) throw new ArgumentNullException(nameof(id));
            _logger.LogInformation("Deleting entity with id {0}", id);
            var result = await Collection.DeleteOneAsync(Filters.Eq("_id", id), ctx);
            if (result.IsAcknowledged && result.DeletedCount == 1) return;
            throw new SibRepositoryException($"Unable to delete entity with id {id}");
        }
    }
}