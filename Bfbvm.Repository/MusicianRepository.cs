﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Bfbvm.Core.Domain;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;

namespace Bfbvm.Repository
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Repository.MongoRepository{T}" />
    public class MusicianRepository : MongoRepository<Musician>, IMusicianRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MusicianRepository" /> class.
        /// </summary>
        /// <param name="logger">The logger.</param>
        /// <param name="connectionString">The connection string.</param>
        public MusicianRepository(ILogger<MusicianRepository> logger, string connectionString) 
            : base(logger, connectionString)
        {
        }

        /// <summary>
        /// Gets all musicians.
        /// </summary>
        /// <param name="ctx">The CTX.</param>
        /// <returns>The list of musicians.</returns>
        public Task<List<Musician>> GetAllMusicians(CancellationToken ctx = default)
        {
            return Collection.Find(Filters.Eq(m => m.Deleted, null)).ToListAsync(ctx);
        }
    }
}